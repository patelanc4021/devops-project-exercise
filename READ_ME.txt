In order to run my ToDo web application, please follow following steps-
1. Please install Python from https://www.python.org/ and follow the setup wizard.
2. Clone this repository `git clone https://gitlab.com/patelanc4021/devops-project-exercise.git`
3. Open terminal inside the repository.
4. Install Flask by running following command - pip install Flask
5. Install Flask-SQLAlchemy by running following command - pip install Flask-SQLAlchemy     
6. Run following command to run the app from the terminal - python app.py
7. Go to your browser and navigate to - http://127.0.0.1:5000/ and enjoy the app
8. To close the app
    a. CTRL+C on your terminal
    b. Close the browser.